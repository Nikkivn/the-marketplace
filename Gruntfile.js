'use strict';

module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        src: 'src',
        dist: 'dist',
        bower_path: 'bower_components',

        sass: {
            options: {
                includePaths: ['<%= bower_path %>/bootstrap-sass/assets/stylesheets',
                    '<%= bower_path %>/font-awesome/scss',
                    '<%= bower_path %>/ionrangeslider/css',
                    '<%= bower_path %>/owl.carousel/dist/assets',
                    '<%= bower_path %>/eonasdan-bootstrap-datetimepicker/build/css',
                    '<%= bower_path %>/tether/dist/css',
                    '<%= src %>/js/vendor']
            },
            dist: {
                options: {
                    outputStyle: 'extended'
                },
                files: {
                    '<%= dist %>/css/app.css': '<%= src %>/scss/app.scss' //,
                    // '<%= src %>/css/ie.css': '<%= src %>/scss/ie.scss'
                }
            }
        },

        concat: {
            options: {
                separator: ';'
            },
            dist: {
                files: {
                    '<%= dist %>/js/app.js': ['<%= src %>/js/app.js'],
                    '<%= dist %>/js/vendor.js': [   '<%= bower_path %>/jquery/dist/jquery.js',
                                                    '<%= bower_path %>/bootstrap-sass/assets/javascripts/bootstrap.js',
                                                    '<%= bower_path %>/bootstrap-spinner/dist/bootstrap-spinner.js',
                                                    '<%= bower_path %>/ionrangeslider/js/ion.rangeSlider.js',
                                                    '<%= bower_path %>/owl.carousel/dist/owl.carousel.js',
                                                    '<%= bower_path %>/moment/moment.js',
                                                    '<%= bower_path %>/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                                                    '<%= bower_path %>/tether/dist/js/tether.js',
                                                    '<%= src %>/js/vendor/navbar-twitch.js',
                                                    '<%= src %>/js/vendor/datatables/datatables.js',
                                                    '<%= src %>/js/vendor/enquire.js',
                                                    '<%= src %>/js/vendor/jquery.jqpagination.js'
                                                ]
                }
            }
        },

        clean: {
            dist: {
                src: ['<%= dist %>/*']
            }
        },
        copy: {
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: '<%= src %>/',
                        src: ['fonts/**', 'img/**', '!**/*.scss', 'js/vendor/datatables/**'],
                        dest: '<%= dist %>/'
                    }
                ]
            }
        },

        uglify: {
            options: {
                preserveComments: 'some',
                mangle: false,
                beautify: (!grunt.option('minify'))
            }
        },

        useminPrepare: {
            html: ['<%= src %>/index.html'],
            options: {
                dest: '<%= dist %>'
            }
        },

        usemin: {
            html: ['<%= dist %>/**/*.html'],
            css: ['<%= dist %>/css/**/*.css'],
            options: {
                dirs: ['<%= dist %>']
            }
        },

        watch: {
            grunt: {
                files: ['Gruntfile.js'],
                tasks: ['sass']
            },
            sass: {
                files: '<%= src %>/scss/**/*.scss',
                tasks: ['sass']
            },
            livereload: {
                files: ['<%= src %>/*.hbs', '<%= src %>/*.json','<%= src %>/partials/**/*.hbs', '<%= src %>/scss/**/*.scss', '<%= src %>/js/**/*.js', '<%= src %>/img/**/*.{jpg,gif,svg,jpeg,png}'],
                tasks: ['compile-dist'],
                options: {
                    livereload: true
                }
            }
        },

        'compile-handlebars': {
            rootFiles: {
                files: [{
                    expand: true,
                    cwd: 'src/',
                    src: '*.hbs',
                    dest: 'dist/',
                    ext: '.html'
                }],
                templateData: 'src/**/*.json',
                partials: 'src/partials/**/*.hbs'
            }
        },

        connect: {
            src: {
                options: {
                    port: 9000,
                    base: '<%= src %>/',
                    open: true,
                    livereload: true,
                    hostname: '127.0.0.1'
                    //hostname: '192.168.1.14'
                }
            },
            dist: {
                options: {
                    port: 9001,
                    base: '<%= dist %>/',
                    open: true,
                    livereload: true,
                    hostname: '127.0.0.1'
                    //hostname: '192.168.1.13'
                }
            }
        }

    });


    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-usemin');
    grunt.loadNpmTasks('grunt-compile-handlebars');
    grunt.loadNpmTasks('grunt-newer');

    grunt.registerTask('compile-sass', ['sass']);
    grunt.registerTask('compile-dist', ['sass', 'compile-handlebars', 'concat:dist', 'copy:dist'])
    grunt.registerTask('default', ['clean:dist', 'compile-dist', 'connect:dist','watch:livereload']);
    grunt.registerTask('publish', ['clean:dist', 'compile-sass', 'handlebars', 'copy:dist', 'concat', 'cssmin', 'uglify']);

};