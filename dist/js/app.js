/*!
 * app.js
 */
$(function () {

    /** --------------------  **/
    /** Initialise Datatables **/
    /** --------------------  **/
    $('#sample').dataTable({
        "bPaginate": false,
        "bInfo": false,
        "bFilter": false
    });
    /** ---------------------------------------- **/

    /** --------------------------------------------- **/
    /** Initialise sample datatables with filter tags **/
    /** --------------------------------------------- **/
    var table = $('#managed-table').DataTable({
        'responsive': true,
        "dom": '<"status-filter">',
        "bPaginate": false
    }).on('click', 'i.delete-row', function (e) {
        e.preventDefault();

        /** Ideally, this will need to move into the 'success' callback for an ajax call **/
        table
            .row($(this).parents('tr'))
            .remove()
            .draw();
    });
    /** ---------------------------------------- **/


    /** ------------------------------------------- **/
    /** Initialise filter event for datatables tags **/
    /** ------------------------------------------- **/
    $('div.status-filter span').on('click', function (e) {
        var value = ($(this).text() == "all" || $(this).text() == "All") ? "" : $(this).text();
        var $tbl = $(this).parent().siblings('table');
        $(this).parent().find('span.badge').removeClass('selected');
        $(this).addClass('selected');
        filterDataTables($tbl, value);
    });
    /** ---------------------------------------- **/

    /** -------------------------- **/
    /** Initialise our "Mega Menu" **/
    /** -------------------------- **/
    $('#search-filter').parent().on('click.bs.megamenu', function (e) {
        e.preventDefault();
        $('div.mega-dropdown-menu').toggleClass('open');
    });
    /** ---------------------------------------- **/

    /** ------------------- **/
    /** Search Autocomplete **/
    /** ------------------- **/
    var searchTimeout, closeTimeout;
    var $autoCompleteContainer = $('div.search-autocomplete');


    $('form.navbar-form input.search-query').on('keyup', function (e) {
        if ($(this).val() == "") {
            return;
        }
        clearTimeout(searchTimeout);
        searchTimeout = setTimeout(function () {
            $autoCompleteContainer.addClass('open');
        }, 500);
    }).on('blur', function (e) {
        closeTimeout = setTimeout(function () {
            $autoCompleteContainer.removeClass('open');
        }, 300);
    }).on('focus', function (e) {
        $('div.mega-dropdown-menu').removeClass('open');
    });
    /** ---------------------------------------- **/

    /** ----------------------------------------- **/
    /** Auto complete drop down clickable results **/
    /** ----------------------------------------- **/
    $(".dropdown-menu.search-autocomplete ul li").click(function () {
        window.location = $(this).find("a").attr("href");
        return false;
    });
    /** ---------------------------------------- **/

    /** ---------------------------------------- **/
    /** Signup page floating labels form pattern **/
    /** ---------------------------------------- **/
        // Test for placeholder support
    $.support.placeholder = (function () {
        var i = document.createElement('input');
        return 'placeholder' in i;
    })();

    // Hide labels by default if placeholders are supported
    if ($.support.placeholder) {

        $('.signup-form div.form-group').find('input').on('keyup blur focus', function (e) {

            // Cache our selectors
            var $this = $(this),
                $parent = $this.parent();

            // Add or remove classes
            if (e.type == 'keyup') {
                if ($this.val() == '') {
                    $parent.addClass('hide-label');
                } else {
                    $parent.removeClass('hide-label');
                }
            }
            else if (e.type == 'blur') {
                if ($this.val() == '') {
                    $parent.addClass('hide-label');
                }
                else {
                    $parent.removeClass('hide-label').addClass('unhighlight-label');
                }
            }
            else if (e.type == 'focus') {
                if ($this.val() !== '') {
                    $parent.removeClass('unhighlight-label');
                }
            }
        });
    } else {
        $('.signup-form div.form-group').each(function () {
            $(this).removeClass('hide-label');
        });
    }
    /** ---------------------------------------- **/

    /** -------------------------------------------------------------------- **/
    /** Toggle between plus/minus glyphicon when clicking on relevant button **/
    /** -------------------------------------------------------------------- **/
    $('.minus-toggleable').on('click', function () {
        $(this).find('span.glyphicon').toggleClass('glyphicon-minus glyphicon-plus');
    });
    /** ---------------------------------------- **/

    /** ---------------------------------------------- **/
    /** Range slider control for product-category page **/
    /** ---------------------------------------------- **/
    $("input.price-range-slider").ionRangeSlider({
        type: "double",
        grid: false,
        min: 3000,
        max: 35000,
        from: 4000,
        to: 8000,
        step: 500,
        hide_min_max: true,
        hide_from_to: false,
        prefix: "R"
    });
    /** ---------------------------------------- **/

    /** -------------------------------------------------------- **/
    /** Product Carousel - for thumbnails on single-product page **/
    /** -------------------------------------------------------- **/
    var $productCarousel = $('div.product-carousel');
    if ($productCarousel.length) {

        var options = {
            margin: 0,
            nav: false,
            responsive: {
                0: {
                    items: 1
                },
                767: {
                    items: 2
                },
                991: {
                    items: 3
                },
                1199: {
                    items: 4
                },
                1599: {
                    items: 5
                }
            }
        };

        if ($productCarousel.hasClass('product-thumbnails')) {
            options.responsive[0].items = 3;
            options.responsive[767].items = 4;
            options.responsive[991].items = 5;
            options.responsive[1199].items = 5;
        }

        $productCarousel.owlCarousel(options);

        $('.owl-next').on('click', function (e) {
            e.preventDefault();
            $productCarousel.trigger('next.owl.carousel');
        });

        // Go to the previous item
        $('.owl-prev').on('click', function (e) {
            e.preventDefault();
            $productCarousel.trigger('prev.owl.carousel');
        });

    }
    /** ---------------------------------------- **/

    /** ------------------- **/
    /** Datepicker controls **/
    /** ------------------- **/
    $('.datepicker').each(function () {
        var prefix = $(this).data('prefix');
        var format = (prefix.length) ? '[' + prefix + '] DD/MM/YYYY' : 'DD/MM/YYYY';
        $(this).datetimepicker({
            format: format,
            widgetPositioning: {horizontal: 'auto', vertical: 'bottom'}
        });
    });
    /** ---------------------------------------- **/


    /** ---------------------------- **/
    /** Settings Pages - "edit" link **/
    /** ---------------------------- **/
    $('table tbody td').on('click', '.btn-edit-settings', function (e) {
        e.preventDefault();
        var tr = $(this).parents('tr');
        tr.find('input').removeAttr('disabled').focus();
    });
    /** ---------------------------------------- **/


    /** ------------------------------------- **/
    /** Quick Add form - product autocomplete **/
    /** ------------------------------------- **/
    var quickAddTimeout, quickCloseTimeout;
    var $quickAddAutoCompleteContainer = $('div.quick-add-autocomplete');


    $('#quick-add-prodcode').on('keyup', function (e) {
        if ($(this).val() == "") {
            return;
        }

        clearTimeout(quickAddTimeout);
        quickAddTimeout = setTimeout(function () {
            $quickAddAutoCompleteContainer.addClass('open');
        }, 500);
    }).on('blur', function (e) {
        quickCloseTimeout = setTimeout(function () {
            $quickAddAutoCompleteContainer.removeClass('open');
        }, 300);
    });
    /** ---------------------------------------- **/

    /** ----------------------------------------- **/
    /** Auto complete drop down clickable results **/
    /** ----------------------------------------- **/
    $quickAddTitle = $('#quick-add-product-title');
    $quickAddPrice = $('#quick-add-product-price');
    $('body').on('click', '.dropdown-menu.quick-add-autocomplete ul li', function () {
        var $this = $(this);
        $quickAddTitle.val($this.data('product-title'));
        $quickAddPrice.text($this.data('product-price'));
    });
    /** ---------------------------------------- **/

    /** -------------------------------- **/
    /** Search results grid view toggles **/
    /** -------------------------------- **/
    $('.content-wrapper').on('click', '.search-grid-controls a', function (e) {
        e.preventDefault();
        var $parent = $(this).parent();

        $parent.find('a').removeClass('selected');
        $(this).addClass('selected');

        jtToggleProductGrid($(this).data('layout'));

    });
    /** ---------------------------------------- **/

    /** ------ **/
    /** Modals **/
    /** ------ **/
    $('.modal-vcenter').on('show.bs.modal', function (e) {
        centerModals($(this));
    });

    $(window).on('resize', centerModals);
    /** ---------------------------------------- **/


    /** -------------------------------- **/
    /** Walkthrough - arrow tethering **/
    /** -------------------------------- **/
    if ($('body.walkthrough').length) {
        $('.pointer').each(function () {
            var options = {
                element: $(this),
                target: $(this).data('target'),
                attachment: $(this).data('attachment'),
                targetAttachment: $(this).data('target-attachment')
            };

            var anchored = new Tether(options);
        });

        $('div.radio').on('click', function (e) {
            var $input = $(this).find('input');
            $('.pointer').removeClass('enabled');
            $('.pointer.' + $input.val()).addClass('enabled');
            $('.blurb div.step').removeClass('enabled');
            $('.blurb div.' + $input.val()).addClass('enabled');
        });

        //$('.pointer.step1').addClass('enabled');

        $('div.controls').on('click', 'button', function (e) {
            e.preventDefault();
            $('div.modal.fade.in').removeClass('in');
            $('div.modal-backdrop.fade.in').removeClass('in');
            $('body').removeClass('modal-open walkthrough');
            $('div.pointer').removeClass('enabled');
        });
    }
    /** ------------------------------------- **/

    /** --------------------- **/
    /** Brand icon checkboxes **/
    /** --------------------- **/
    $('div.preferred-brands').on('click', 'i', function () {
        var relatedCheckBox = $('#' + $(this).data('checkbox'));
        (relatedCheckBox.attr('checked')) ? relatedCheckBox.removeAttr('checked') : relatedCheckBox.attr('checked', 'checked');
        $(this).toggleClass('checked');
    });

    /** --------------------------------------- **/
    /** Init our breakpoints **/
    /** --------------------------------------- **/
    jtRegisterBreakpointListner();

    /** ------------------------------------- **/

    /** --------------------- **/
    /** jqPagination **/
    /** --------------------- **/
    $('.jq-pagination').jqPagination({
        link_string	: '/?page={page_number}',
        max_page	: 40,
        paged		: function(page) {
            //put ajax request to server side page here
        }
    });

});


/* Vertically center modals */
function centerModals($element) {
    var $modals;
    if ($element.length) {
        $modals = $element;
    } else {
        $modals = $('.modal-vcenter:visible');
    }
    $modals.each(function (i) {
        var $clone = $(this).clone().css('display', 'block').appendTo('body');
        var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
        top = top > 0 ? top : 0;
        $clone.remove();
        $(this).find('.modal-content').css("margin-top", top);
    });
}


/** Filter datatables instance based on value supplied **/
function filterDataTables($tbl, val) {

    var table = $tbl.DataTable();
    if (val == '' || val == 'All') {
        table.search('').columns().search('').draw();
    } else {
        table.search(val, true, true).draw();
    }
}

function jtToggleProductGrid(cols) {
    var grid = 'col-xs-';

    switch (cols) {
        case 2:
            grid += '6';
            break;
        case 3:
            grid += '4';
            break;
        case 4:
            grid += '3';
            break;
        default:
            grid += '12';
            break;
    }

    $('div.products').find('div.col').each(function () {
        $(this).removeClass('col-xs-12').removeClass('col-xs-6').removeClass('col-xs-4').removeClass('col-xs-3').addClass(grid);
        if (cols <= 1) {
            $(this).find('.col-img').removeClass('col-xs-12').addClass('col-xs-12 col-sm-3');
            $(this).find('.col-details').removeClass('col-xs-12').addClass('col-xs-12 col-sm-5');
            $(this).find('.col-controls').removeClass('col-xs-12').addClass('col-xs-12 col-sm-4');
        } else {
            $(this).find('.col-img').removeClass('col-xs-12 col-sm-3').addClass('col-xs-12');
            $(this).find('.col-details').removeClass('col-xs-12 col-sm-5').addClass('col-xs-12');
            $(this).find('.col-controls').removeClass('col-xs-12 col-sm-4').addClass('col-xs-12');
        }
    });
}


function jtRegisterBreakpointListner() {

    enquire.register("screen and (max-width: 993px)", {
        match: function () {
            $('body').removeClass('sidebar-open');
        },

        unmatch: function () {
            $('body').addClass('sidebar-open');
        }
    });
}