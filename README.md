# The Marketplace Distribution Build

Source and distribution build for The Marketplace frontend

## Requirements

Install the following items before continuing:

  * [Node.js](http://nodejs.org): Use the installer provided on the NodeJS website.
  * [Grunt](http://gruntjs.com/): Run `[sudo] npm install -g grunt-cli`
  * [Bower](http://bower.io): Run `[sudo] npm install -g bower`

## Quickstart

```bash
git clone https://bitbucket.org/Nikkivn/the-marketplace
```

```bash
cd the-marketplace/
```

```bash
npm install && bower install
```

While you're working on your project, run:

`grunt`

To build a 'distribution' ready version (unminified JS), run:

`grunt publish`

To build a 'distribution' ready version WITH minified JS, run:

`grunt publish --minify`

And you're set!

## Directory Structure
  * `src/` : main source folder
  * `src/scss/_custom-variables.scss`: Configuration settings
  * `src/scss/app.scss`: import application styles
  * `src/js/custom`: Custom plugins written specifically for this project
  * `src/js/app.js`: Main app JS file. Insert custom JS here if required
  * `src/js/app-ie.js`: IE specific JS (where applicable)
  * `src/js/vendor`: Vendor plugins.

## Important Notes

Only edit source files in the `src/` folder. Changes made here are tracked by the repository.


```>
grunt publish
```

will create/overwrite a `dist/` folder which contains the distribution code.