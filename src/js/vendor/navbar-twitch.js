$(document).ready(function() {
    $('.navbar-twitch-toggle').on('click', function(event) {
        event.preventDefault();
        var action = ($('body').hasClass('sidebar-open')) ? 'enable' : 'disable';
        $('.navbar-nav [data-toggle="tooltip"]').tooltip(action);

        $('body').toggleClass('sidebar-open');
    });
});